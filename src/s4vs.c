/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/cstr.h>
#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>

#include <star/s3d.h>
#include <star/s3daw.h>
#include <star/smc.h>

#include "s4vs_realization.h"

static res_T
compute_4v_s
  (const size_t max_realisations,
   const double x0,
   const int flag,
   const double ke,
   const double alpha,
   const double a)
{
  char dump[64];
  struct s4vs_context ctx;
  struct smc_device* smc = NULL;
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;
  struct smc_device* smc2 = NULL;
  struct smc_integrator integrator2;
  struct smc_estimator* estimator2 = NULL;
  struct smc_estimator_status estimator_status2;
  double analytical_L;
  double analytical_dL;
  double delta = 0.001;
  double a_0 = a-0.001;
  double a_1 = a+0.001;

  struct time t0, t1;
  res_T res = RES_OK;
  FILE *fp = NULL;

  ASSERT(max_realisations > 0 && ks >= 0);
  ASSERT(max_realisations > 0 && ks >= 0);

  /* Initialize context for MC computation */
  ctx.x0 = x0;
  ctx.flag = flag;
  ctx.ke = ke;
  ctx.alpha = alpha;
  ctx.a = a;

  /* Setup Star-MC */
  SMC(device_create(NULL, NULL, SMC_NTHREADS_DEFAULT, SSP_RNG_KISS, &smc));
  integrator.integrand = &s4vs_realization; /* Realization function */
  integrator.type = &smc_double; /* Type of the Monte Carlo weight */
  integrator.max_realisations = max_realisations; /* Realization count */
  integrator.max_failures = max_realisations / 1000;

  /* Solve */
  time_current(&t0);
  SMC(solve(smc, &integrator, &ctx, &estimator));
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  printf("Computation time: %s\n", dump);

  /* Now here we go for Maxime Roger method */
  /* Setup Star-MC */
  integrator2.integrand = &s4vs_realization2; /* Realization function */
  integrator2.type = &smc_double; /* Type of the Monte Carlo weight */
  integrator2.max_realisations = max_realisations; /* Realization count */
  integrator2.max_failures = max_realisations / 1000;

  /* Solve */
  time_current(&t0);
  SMC(solve(smc, &integrator2, &ctx, &estimator2));
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  printf("Computation time: %s\n", dump);

  /* Print the simulation results */
  SMC(estimator_get_status(estimator, &estimator_status));
  SMC(estimator_get_status(estimator2, &estimator_status2));

   if(estimator_status.NF > integrator.max_failures) {
    fprintf(stderr,
	    "Too many failures (%lu). The scene might not match the prerequisites:\n"
	    "it must be closed and its normals must point *into* the volume.\n",
	    (unsigned long)estimator_status.NF);
    goto error;
  }

  /* NOTE 3 sigma is outputed */
  switch (flag){

    case 0:
      analytical_L = 
        (-alpha*sqrt(1-alpha)*sinh((a-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a*sqrt(1-alpha)*ke));
      analytical_dL = 
        ((-alpha*sqrt(1-alpha)*sinh((a_1-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a_1*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a_1*sqrt(1-alpha)*ke))
        -(-alpha*sqrt(1-alpha)*sinh((a_0-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a_0*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a_0*sqrt(1-alpha)*ke)))/delta/2;

      printf("LL(%g) = %g +/- %g ~ %g\n#failures = %lu/%lu\n",
       x0,
       SMC_DOUBLE(estimator_status.E),
       /* NOTE 3 sigma is outputed */
       SMC_DOUBLE(estimator_status.SE)*3,
       analytical_L,
             (unsigned long)estimator_status.NF,
       (unsigned long)max_realisations);
      printf("dLL(%g) = %g +/- %g ~ %g\n#failures = %lu/%lu\n",
       x0,
       SMC_DOUBLE(estimator_status2.E),
       SMC_DOUBLE(estimator_status2.SE)*3,
       analytical_dL,
             (unsigned long)estimator_status2.NF,
       (unsigned long)max_realisations);

      fp = fopen("../output/res_rod_1d_LL.txt","a+");
      fprintf(fp,"%g %g %g %g %g\n",x0,
       SMC_DOUBLE(estimator_status.E),
       SMC_DOUBLE(estimator_status.SE)*3,
       SMC_DOUBLE(estimator_status2.E),
       SMC_DOUBLE(estimator_status2.SE)*3);
      fclose(fp);  
      break;
    case 1:
      analytical_L = 
        (2*(-1+alpha)*cosh((a-x0)*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh((a-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a*sqrt(1-alpha)*ke));
      analytical_dL = 
        ((2*(-1+alpha)*cosh((a_1-x0)*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh((a_1-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a_1*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a_1*sqrt(1-alpha)*ke))
        -(2*(-1+alpha)*cosh((a_0-x0)*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh((a_0-x0)*sqrt(1-alpha)*ke))/(2*(-1+alpha)*cosh(a_0*sqrt(1-alpha)*ke)+sqrt(1-alpha)*(-2+alpha)*sinh(a_0*sqrt(1-alpha)*ke)))/delta/2;

      printf("LR(%g) = %g +/- %g ~ %g\n#failures = %lu/%lu\n",
       x0,
       SMC_DOUBLE(estimator_status.E),
       SMC_DOUBLE(estimator_status.SE)*3,
       analytical_L,
             (unsigned long)estimator_status.NF,
       (unsigned long)max_realisations);
      printf("dLR(%g) = %g +/- %g ~ %g\n#failures = %lu/%lu\n",
       x0,
       SMC_DOUBLE(estimator_status2.E),
       SMC_DOUBLE(estimator_status2.SE)*3,
       analytical_dL,
             (unsigned long)estimator_status2.NF,
       (unsigned long)max_realisations);

      fp = fopen("../output/res_rod_1d_LR.txt","a+");
      fprintf(fp,"%g %g %g %g %g\n",x0,
       SMC_DOUBLE(estimator_status.E),
       SMC_DOUBLE(estimator_status.SE)*3,
       SMC_DOUBLE(estimator_status2.E),
       SMC_DOUBLE(estimator_status2.SE)*3);
      fclose(fp);  
      break;
  }


exit:
  /* Clean-up data */
  if(smc) SMC(device_ref_put(smc));
  if(smc2) SMC(device_ref_put(smc2));
  if(estimator) SMC(estimator_ref_put(estimator));
  if(estimator2) SMC(estimator_ref_put(estimator2));
  return res;

error:
  goto exit;
}

int
main(int argc, char* argv[])
{
  unsigned long nrealisations = 1000000;
  double x0 = 0.0;
  double alpha = 0.0;
  double ke = 0.0;
  double a = 0.0;
  res_T res = RES_OK;
  int err = 0;
  int flag = 0;

  /* Check command arguments */
  if(argc <= 1 ) {
    printf("Usage: %s SAMPLES_COUNT X_INITIAL FLAG_LL(0)_LR(1) K_E ALPHA A_LENGTH\n", argv[0]);
    goto error;
  }

  /* Set number of realizations */
  if(argc >= 2) {
    res = cstr_to_ulong(argv[1], &nrealisations);
    if(nrealisations <= 0 || res != RES_OK) {
      fprintf(stderr, "Invalid number of realisations `%s'\n", argv[1]);
      goto error;
    }
  }

  /* Set x0 */
  if(argc >= 3) {
    res = cstr_to_double(argv[2], &x0);
    if(res != RES_OK || x0 < 0) {
      fprintf(stderr, "Invalid x0 `%s'\n", argv[2]);
      goto error;
    }
  }

  /* flag for LL/LR */
  if(argc >= 4) {
    res = cstr_to_int(argv[3], &flag);
    if(nrealisations <= 0 || res != RES_OK) {
      fprintf(stderr, "Invalid flag LL for 0/LR for 1 `%s'\n", argv[3]);
      goto error;
    }
  }

  /* Set ke */
  if(argc >= 5) {
    res = cstr_to_double(argv[4], &ke);
    if(res != RES_OK || ke < 0) {
      fprintf(stderr, "Invalid ke value `%s'\n", argv[4]);
      goto error;
    }
  }

  /* Set alpha */
  if(argc >= 6) {
    res = cstr_to_double(argv[5], &alpha);
    if(res != RES_OK || x0 < 0) {
      fprintf(stderr, "Invalid alpha `%s'\n", argv[5]);
      goto error;
    }
  }

  /* Set a */
  if(argc >= 7) {
    res = cstr_to_double(argv[6], &a);
    if(res != RES_OK || x0 < 0) {
      fprintf(stderr, "Invalid a_length `%s'\n", argv[6]);
      goto error;
    }
  }

  res = compute_4v_s(nrealisations, x0, flag, ke, alpha, a);
  if(res != RES_OK) {
    fprintf(stderr, "Error in 4V/S integration\n");
    goto error;
  }

exit:
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
    err = 1;
  }
  return err;
error:
  err = 1;
  goto exit;
}

