# 1d_rod_sensitivity_MCM_modeling

MCM modeling of observable and sensitivity for a 1d-rod analytical radiative problem.

This project is developped based on the Star-4V/S project: https://gitlab.com/meso-star/star-4v_s.git

## How to build

This project relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build. It also
depends on the
[Star-Engine](https://gitlab.com/meso-star/star-engine) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned prerequisites. Then generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directory of its dependencies.

	$ cd ./1d_rod_sensitivity_MCM_modeling
	$ mkdir ./build && cd ./build
	$ cmake ../cmake -DCMAKE_PREFIX_PATH={PATH-FOR-STARENGINE} && make

## How to run

	$ ./bash/script_rod1D.py
