#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import math
import os
import subprocess
from scipy.misc import derivative

# create ../output to store res files
flag = os.path.exists("../output")
if (~flag):
    subprocess.call("mkdir ../output",shell=True)

#clean the old res files
subprocess.call("rm -fr ../output/*",shell=True)

def LR(x,a):
    return (2*(-1+alpha)*math.cosh((a-x)*math.sqrt(1-alpha)*sigma_t) + math.sqrt(1-alpha)*(-2+alpha)*math.sinh((a-x)*math.sqrt(1-alpha)*sigma_t))/(2*(-1+alpha)*math.cosh(a*math.sqrt(1-alpha)*sigma_t)+math.sqrt(1-alpha)*(-2+alpha)*math.sinh(a*math.sqrt(1-alpha)*sigma_t))

def LL(x,a):
    return -math.sqrt(1-alpha)*alpha*math.sinh((a-x)*math.sqrt(1-alpha)*sigma_t)/(2*(-1+alpha)*math.cosh(a*math.sqrt(1-alpha)*sigma_t)+math.sqrt(1-alpha)*(-2+alpha)*math.sinh(a*math.sqrt(1-alpha)*sigma_t))

def dLR(x,a):
    a1 = a-0.01
    a2 = a+0.01
    return (LR(x,a2)-LR(x,a1))/(a2-a1)

def dLL(x,a):
    a1 = a-0.01
    a2 = a+0.01
    return (LL(x,a2)-LL(x,a1))/(a2-a1)

#user input
a = 2
lower = 0
upper = a
step = 0.1
pi_pp = 2
alpha = 0.7 # kd/ke
#sigma_t = 3 # ke
sigma_t = 0.1 # ke

N = [50000,100000,200000,400000,800000,1600000,3200000,6400000,12800000]

## analytical solution
#R = np.arange(lower,upper+step,step)
#R[-1] = R[-1]-0.01
#LR_exact = np.zeros(len(R))
#LL_exact = np.zeros(len(R))
#dLR_exact = np.zeros(len(R))
#dLL_exact = np.zeros(len(R))

#write the titles for .tex
f = open("../output/res_rod_1d_LL.txt", 'a')
f.write('x0 LLE LLSE SLE SLSE\n')
f.close()

f = open("../output/res_rod_1d_LR.txt", 'a')
f.write('x0 LRE LRSE SRE SRSE\n')
f.close()

# the test point: x0=1.9
x0 = 1.99
# analytical solutions for obervable
LR_exact = LR(x0,pi_pp)
LL_exact = LL(x0,pi_pp)
dLR_exact = dLR(x0,pi_pp) 
dLL_exact = dLL(x0,pi_pp) 
for i in range(len(N)):

    nb_realization = N[i]
    # MCM solution for obervable 
    #SAMPLES_COUNT X_INITIAL FLAG_LL_LR K_E ALPHA A_LENGTH
    print(subprocess.call(["../build/s4vs",str(nb_realization),str(x0),str(0),str(sigma_t),str(alpha),str(a)],shell=False))  
    print(subprocess.call(["../build/s4vs",str(nb_realization),str(x0),str(1),str(sigma_t),str(alpha),str(a)],shell=False))  

#for i in range(len(R)):
#    # analytical solutions for obervable
#    LR_exact[i] = LR(R[i],pi_pp)
#    LL_exact[i] = LL(R[i],pi_pp)
#    # MCM solution for obervable 
#    #SAMPLES_COUNT X_INITIAL FLAG_LL_LR K_E ALPHA A_LENGTH
#    print(subprocess.call(["../build/s4vs",str(nb_realization),str(R[i]),str(0),str(sigma_t),str(alpha),str(a)],shell=False))  
#    print(subprocess.call(["../build/s4vs",str(nb_realization),str(R[i]),str(1),str(sigma_t),str(alpha),str(a)],shell=False))  
#    # analytical solution for sensitivity dO/da
#    dLR_exact[i] = dLR(R[i],pi_pp) 
#    dLL_exact[i] = dLL(R[i],pi_pp) 

##write exact solutions for .tex
#f = open("../output/exact_rod_1d_LL.txt", 'a')
#f.write('x0 LL SL\n')
#for i in range(len(R)):
#    f.write(str(R[i])+" "+str(LL_exact[i])+" "+str(dLL_exact[i])+'\n')
#f.close()
#
#f = open("../output/exact_rod_1d_LR.txt", 'a')
#f.write('x0 LR SR\n')
#for i in range(len(R)):
#    f.write(str(R[i])+" "+str(LR_exact[i])+" "+str(dLR_exact[i])+'\n')
#f.close()

# read res
res = np.loadtxt("../output/res_rod_1d_LL.txt",skiprows=1) 
LL = res[:,1]
LL_err = res[:,2]
dLL = res[:,3]
dLL_err = res[:,4]

res = np.loadtxt("../output/res_rod_1d_LR.txt",skiprows=1) 
LR = res[:,1]
LR_err = res[:,2]
dLR = res[:,3]
dLR_err = res[:,4]

##plot the results
#fig1 = plt.figure('Figure1',figsize = (6,4)).add_subplot(111)
#fig1.plot(R,LR_exact,label='analytical LR')
#fig1.plot(R,LL_exact,label='analytical LL')
#fig1.errorbar(R,LL,yerr=3*LL_err,fmt='.',ecolor='r',color='r',elinewidth=2,capsize=4,label='MCM LL')
#fig1.errorbar(R,LR,yerr=3*LR_err,fmt='.',ecolor='b',color='b',elinewidth=2,capsize=4,label='MCM LR')
##fmt :   'o' ',' '.' 'x' '+' 'v' '^' '<' '>' 's' 'd' 'p'
#fig1.set_xlabel('x [m]')
#fig1.set_ylabel('L [w/m^2 sr]')
#fig1.set_title('Radiance'+'(N_realization ='+str(nb_realization)+')')
#plt.legend(loc='best')
#plt.savefig('../output/rod_1d_obs.pdf')
print(LL_exact)
print(dLL_exact)

fig2 = plt.figure('Figure2',figsize = (6,4)).add_subplot(111)
#fig2.plot(R,dLR_exact,label='analytical dLL')
#fig2.plot(R,dLL_exact,label='analytical dLR')
fig2.errorbar(N,dLL,yerr=3*dLL_err,fmt='.',ecolor='r',color='r',elinewidth=2,capsize=4,label='MCM dLL(x)')
fig2.errorbar(N,dLR,yerr=3*dLR_err,fmt='.',ecolor='b',color='b',elinewidth=2,capsize=4,label='MCM dLR(x)')
fig2.set_xlabel('x [m]')
fig2.set_ylabel('dL(x) [w/m^3 sr]')
fig2.set_title('sensitivity'+'(N_realization ='+str(nb_realization)+')')
plt.legend(loc='best')
plt.xscale("log")
plt.savefig('../output/rod_1d_sens.pdf')


plt.show()
plt.close()
