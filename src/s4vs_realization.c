/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/float3.h>

#include <star/s3d.h>
#include <star/ssp.h>
#include <star/smc.h>

#include "s4vs_realization.h"

/*******************************************************************************
 * Helper function
 ******************************************************************************/
int
  s4vs_discard_self_hit
(const struct s3d_hit* hit,
 const float ray_org[3],
 const float ray_dir[3],
 void* ray_data,
 void* filter_data)
{
  const struct s3d_primitive* prim_from = ray_data;

  /* Avoid unused variable warn */
  (void)ray_org, (void)ray_dir, (void)filter_data;
  return prim_from ? S3D_PRIMITIVE_EQ(prim_from, &hit->prim) : 0;
}

  static INLINE float
is_positive(double r)
{
  if(r<0.5) return 1.f;
  if(r>0.5) return -1.f;
  printf("ERR: u sampled as 0");
  return 0;
} 

  static INLINE double*
get_d(double d[100], const double r[100], const double u[100], const double x[100], double a, int i)
{
  int j;
  d[0] = x[0]*u[0]-a*(u[0]-1)/2;
  /*calculate d*/
  FOR_EACH(j,1,i+1){ /*j=1 ~ j=i*/
    int k = 0;
    double temp = 0;
    FOR_EACH(k,0,j){
      temp += -r[k]*u[k];
    }
    d[j] = (x[0]+temp)*u[j]-a*(u[j]-1)/2;
  }
  return d;
} 

  static INLINE double*
get_v(double v[100], const double d[100], const double r[100], const double u[100], const double x[100], double a, int i)
{
  int j;
  double temp0 = 0;
  (void) a;
  (void) x;
  /* the first component of v */
  v[0] = -(u[0]-1)*r[0]/2/d[0];
  /* the jth  component of v */
  FOR_EACH(j,1,i){ /*j=1 ~ j=i-1*/
    int k;
    double temp = 0;
    FOR_EACH(k,0,j){ /*k=0 ~ k=j-1*/
      temp += (-u[k]*u[j]*v[k]);
    }
    v[j] = (-(u[j]-1)/2+temp)*r[j]/d[j];
  }
  /* the last component of v */
    FOR_EACH(j,0,i){ /*j=0 ~ j=i-1*/
      temp0 += (-u[j]*u[i]*v[j]);
    }
    v[i] = -(u[i]-1)/2 + temp0;
  return v;
} 

  static INLINE double
get_div(double div[100], const double v[100], const double d[100], const double r[100], const double u[100], const double x[100], const double a, const int i, const double ka, const double Leq, const double length, const double ks)
{
  int j;
  double sum = 0;
  (void) u;
  (void) x;
  (void) d;
  (void) a;
  FOR_EACH(j,0,i){ /* j=0 ~ j=i-1*/
    div[j] = 
      -ka*Leq*exp(-ka*length)*v[j] 
      -ka*Leq*exp(-ka*length)*v[j]*(-u[j]*u[i]) 
      -ks*Leq*exp(-ka*length)*v[j]
      +Leq*exp(-ka*length)*(v[j]/r[j]);
    sum += div[j];
  /*printf("value = %f\n",Leq*exp(-ka*length)*(v[j]/r[j]));*/
  }
  div[i] = -ks*Leq*exp(-ka*length)*v[i];
  sum += div[i];
  return sum;
} 

/*******************************************************************************
 * 4V/S integrand
 ******************************************************************************/
res_T
  s4vs_realization
(void* out_length, struct ssp_rng* rng, const unsigned ithread, void* context)
{
  struct s4vs_context* ctx = (struct s4vs_context*)context;
  float r0,r1;
  double x;
  /* inputs */
  double x0 = ctx->x0;
  double a = ctx->a;
  double w = 0;
  double alpha = ctx->alpha;
  /*float ke = (float)ctx->ke;*/
  double ka = ctx->ke*(1-alpha);
  float ks = (float)(ctx->ke*alpha);
  double LR_0 = 1;
  double LL_a = 0;
  double length = 0;

  int keep_running = 1;
  int flag = LR;
  FILE *fp = NULL;
  (void) *fp;
  (void)ithread; /* Avoid "unused variable" warning */

  /* initialize the x position*/
  x = x0;

  /* Caculate LL or LR ? */
  switch (ctx->flag){
    case 0:
      flag = LL;
      break;
    case 1:
      flag = LR;
      break;
  }

  /* here we go */
  while(keep_running){
    /* LL or LR ? */
    switch (flag){
      case LR:
        r0 = ssp_ran_exp_float(rng,ks);
        if(r0>x){ /* reach boundary condition */
          length += x;
          /*w = factor*LR_0;*/
          w = LR_0*exp(-length*ka);
          keep_running = 0;
        } else{
          x = x - r0;
          length += r0;
          r1 = ssp_rng_canonical_float(rng);
          if(r1<0.5f){
            flag = LR;
          }else{
            flag = LL;
          }
        }
        break;
      case LL:
        r0 = ssp_ran_exp_float(rng,ks);
        if(r0>(a-x)){ /* reach boundary condition */
          length += (a-x);
          /*w = factor*LL_a;*/
          w = LL_a*exp(-length*ka);
          keep_running = 0;
        } else{
          x = x + r0;
          length += r0;
          r1 = ssp_rng_canonical_float(rng);
          if(r1<0.5f){
            flag = LR;
          }else{
            flag = LL;
          }
        }
        break;
    }
  }

  SMC_DOUBLE(out_length) = w;
  return RES_OK;
}

/* sensitivity */
res_T
  s4vs_realization2
(void* out_length, struct ssp_rng* rng, const unsigned ithread, void* context)
{
  struct s4vs_context* ctx = (struct s4vs_context*)context;
  float r0,r1;
  double x;
  double factor = 1;
  /* inputs */
  double x0 = ctx->x0;
  double a = ctx->a;
  double w = 0;
  double alpha = ctx->alpha;
  double dLR_0 = 0;
  double LR_0 = 1;
  double LL_a = 0;
  float ke = (float)ctx->ke;
  /* divers*/
  int keep_running = 1;
  int flag = dLR;
  FILE *fp = NULL;
  (void) *fp;
  (void)ithread; /* Avoid "unused variable" warning */

  /* initialize the x position*/
  x = x0;

  /* Caculate LL or LR ? */
  switch (ctx->flag){
    case 0:
      flag = dLL;
      break;
    case 1:
      flag = dLR;
      break;
  }

  /* here we go for sensitivity */
  while(keep_running){
    /* LL or LR ? */
    switch (flag){
      case dLR:
        r0 = ssp_ran_exp_float(rng,ke);
        if(r0>x){ /* reach boundary condition */
          w = factor * dLR_0;
          keep_running = 0;
        } else{
          r1 = ssp_rng_canonical_float(rng);
          if(r1<0.5f){
            flag = dLR;
            factor = factor*alpha;
            x = x - r0;
          }else{
            flag = dLL;
            factor = factor*alpha;
            x = x - r0;
          }
        }
        break;
      case dLL:
        r0 = ssp_ran_exp_float(rng,ke);
        if(r0>(a-x)){ /* reach boundary condition */
          factor = factor * ke*alpha/2;
          keep_running = 0;
          flag = LR;
        } else{
          r1 = ssp_rng_canonical_float(rng);
          if(r1<0.5f){
            flag = dLR;
            factor = factor*alpha;
            x = x + r0;
          }else{
            flag = dLL;
            factor = factor*alpha;
            x = x + r0;
          }
        }
        break;
    }
  }

  /* find LR */
  if (flag == LR){
    x = a;
    keep_running = 1;
    while(keep_running){
      /* LL or LR ? */
      switch (flag){
        case LR:
          r0 = ssp_ran_exp_float(rng,ke);
          if(r0>x){ /* reach boundary condition */
            w = factor * LR_0;
            keep_running = 0;
          } else{
            r1 = ssp_rng_canonical_float(rng);
            if(r1<0.5f){
              flag = LR;
              factor = factor*alpha;
              x = x - r0;
            }else{
              flag = LL;
              factor = factor*alpha;
              x = x - r0;
            }
          }
          break;
        case LL:
          r0 = ssp_ran_exp_float(rng,ke);
          if(r0>(a-x)){ /* reach boundary condition */
            w = factor * LL_a;
            keep_running = 0;
          } else{
            r1 = ssp_rng_canonical_float(rng);
            if(r1<0.5f){
              flag = LR;
              factor = factor*alpha;
              x = x + r0;
            }else{
              flag = LL;
              factor = factor*alpha;
              x = x + r0;
            }
          }
          break;
      }
    }

  }

  SMC_DOUBLE(out_length) = w;
  return RES_OK;
}

res_T
  s4vs_realization3
(void* out_length, struct ssp_rng* rng, const unsigned ithread, void* context)
{
  struct s4vs_context* ctx = (struct s4vs_context*)context;
  float r0,r1;
  double x[100] = {0};
  double r[100] = {0};
  double u[100] = {0};
  double d[100] = {0};
  double v[100] = {0};
  double div[100] = {0};
  /* inputs */
  double x0 = ctx->x0;
  double a = ctx->a;
  double w = 0;
  double alpha = ctx->alpha;
  /*float ke = (float)ctx->ke;*/
  double ka = ctx->ke*(1-alpha);
  float ks = (float)(ctx->ke*alpha);
  /*double LR_0 = 1;*/
  /*double LL_a = 0;*/
  double length = 0;
  int i = 0;
  int j = 0;

  int keep_running = 1;
  int flag = LR;
  FILE *fp = NULL;
  (void) *fp;
  (void)ithread; /* Avoid "unused variable" warning */

  /* initialize the x position*/
  x[0] = x0;

  /* Caculate LL or LR ? */
  switch (ctx->flag){
    case 0:
      flag = LL;
      u[0] = -1.;
      break;
    case 1:
      flag = LR;
      u[0] = 1.;
      break;
  }

  /* here we go */
  while(keep_running){
    /* LL or LR ? */
    switch (flag){
      case LR:
        r0 = ssp_ran_exp_float(rng,ks);
        r[i] = r0;
        if(r0>x[i]){ /* reach boundary condition */
          double Leq = 1.;
          double sum_div = 0;
          length += x[i];
          /* sensitivity */
          get_d(d,r,u,x,a,i);
          get_v(v,d,r,u,x,a,i);
          sum_div = get_div(div,v,d,r,u,x,a,i,ka,Leq,length,ks);
          w = sum_div;
          /*w = exp(-ka*length);*/
          keep_running = 0;
        } else{
          x[i+1] = x[i] - r0;
          i++;
          length += r0;
          r1 = ssp_rng_canonical_float(rng);
          u[i] = is_positive(r1);
          if(r1<0.5f){
            flag = LR;
          }else{
            flag = LL;
          }
        }
        break;
      case LL:
        r0 = ssp_ran_exp_float(rng,ks);
        r[i] = r0;
        if(r0>(a-x[i])){ /* reach boundary condition */
          double Leq = 0;
          double sum_div = 0;
          length += (a-x[i]);
          /* sensitivity */
          get_d(d,r,u,x,a,i);
          get_v(v,d,r,u,x,a,i);
          sum_div = get_div(div,v,d,r,u,x,a,i,ka,Leq,length,ks);
          w = sum_div;
          /*w = 0; */
          keep_running = 0;
        } else{
          x[i+1] = x[i] + r0;
          i++;
          length += r0;
          r1 = ssp_rng_canonical_float(rng);
          u[i] = is_positive(r1);
          if(r1<0.5f){
            flag = LR;
          }else{
            flag = LL;
          }
        }
        break;
    }
  }

  /*printf("ka,ks = %f,%f\n",ka,ks);*/
  /* check propagateur */
  FOR_EACH(j,0,i+1){
    /*printf("i = %d, x = %f, r = %f, u = %f, d=%f, v=%f, div = %f\n",j,x[j],r[j],u[j],d[j],v[j],div[j]);*/
  }

  /*printf("weigth = %g\n",w);*/
  SMC_DOUBLE(out_length) = w;
  return RES_OK;
}


